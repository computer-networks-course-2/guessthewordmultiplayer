package com.bckho;

import java.util.*;

public class Game {
    private String word;
    private String mysteryWordState = "";
    private List<Character> attemptedLetters;
    private List<String> attemptedWords;

    public Game(String word) {
        this.word = word;
        this.attemptedLetters = new ArrayList<>();
        this.attemptedWords = new ArrayList<>();
        initMysteryWord();
        System.out.println("Chosen word: " + getWord());
    }

    // Encrypt the mystery word
    private void initMysteryWord() {
        for (int i = 0; i < this.word.length(); i++) {
            this.mysteryWordState += "#";
        }
    }

    // Check if char from attempt exists in word
    private boolean letterFromAttemptDoesExistsInWord(Attempt a) {
        char c = a.getLetterGuessed();
        boolean doesContain = false;
        for (int i = 0; i < this.word.length(); i++) {
            if (Character.toLowerCase(c) == this.word.charAt(i)) {

                StringBuilder newMysteryWord = new StringBuilder(this.mysteryWordState);
                newMysteryWord.setCharAt(i, Character.toLowerCase(c));

                this.mysteryWordState = newMysteryWord.toString();

                doesContain = true;
            }
        }

        return doesContain;
    }

    // Check if guessWord matches
    private boolean AttemptWordIsCorrect(Attempt a) {
        if (this.word.equals(a.getWordGuessed())) {
            this.mysteryWordState = a.getWordGuessed();
            return true;
        }

        return false;
    }

    public Attempt checkAttemptAndReturn(Attempt attempt) {
        // Check if attempt's word or letter is not empty
        if (attempt.getLetterGuessed() != ' ' && attempt.getWordGuessed().isEmpty()) {
            if (letterFromAttemptDoesExistsInWord(attempt)) {
                attempt.setHasGuessedTheCorrectLetter(true);

                // Check if the attempt has guessed the last letter of the mystery word
                if (!mysteryWordState.contains("#")) {
                    attempt.setHasGuessedTheCorrectWord(true);
                }

            } else {
                attempt.setHasGuessedTheCorrectLetter(false);
            }
            attempt.setMysteryWord(this.mysteryWordState);

        } else if (attempt.getWordGuessed() != null && attempt.getLetterGuessed() == ' ') {
            attempt.setHasGuessedTheCorrectWord(AttemptWordIsCorrect(attempt));
            attempt.setMysteryWord(this.mysteryWordState);
        }
        System.out.println("Guess status: " + this.mysteryWordState);

        return attempt;
    }

    public boolean wordHasBeenGuessed() {
        return this.mysteryWordState.equals(this.word);
    }

    public String getWord() {
        return this.word;
    }

    public String getMysteryWordState() {
        return this.mysteryWordState;
    }

    public List<Character> getAttemptedLetters() {
        return this.attemptedLetters;
    }

    public List<String> getAttemptedWords() {
        return this.attemptedWords;
    }

    public int getAttemptedLettersCount() {
        return this.attemptedLetters.size();
    }

    public int getAttemptedWordsCount() {
        return this.attemptedWords.size();
    }
}
