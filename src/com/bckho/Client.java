package com.bckho;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {
    Socket socket;
    Scanner scanner;

    private String username = "";

    private Client(InetAddress serverAddress, int serverPort) throws Exception {
        this.socket = new Socket(serverAddress, serverPort);
        this.scanner = new Scanner(System.in);
    }

    private void askAndSetUsername() {
        while (true) {
            System.out.println("Welcome to GuessTheWordMultiplayer, please enter your username:");

            String clientUsername = this.scanner.nextLine();
            if (!clientUsername.isEmpty()) {
                this.username = clientUsername;
                break;
            } else {
                System.out.println("Username is empty, please enter a valid username.");
            }
        }
    }

    private void printCommands() {
        System.out.println("\r\nEnter one of the following commands:" +
                "\nA: guess a letter | B: guess the word");
    }

    private void start() throws IOException, ClassNotFoundException {
        Attempt attemptData;
        String command;
        char letter;
        String guessWord;

        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());

        while (true) {
            letter = ' ';
            guessWord = "";
            command = "";

            // If username is empty, then client is asked to enter a username
            if (this.username.isEmpty()) {
                askAndSetUsername();
            }

            printCommands();

            command = scanner.nextLine();

            // Check command
            if (command.toLowerCase().equals("a")) {
                // Single character attempt of user
                System.out.println("\r\nEnter a single character: ");
                char letterInput = this.scanner.nextLine().charAt(0);

                if (letterInput != ' ') {
                    letter = letterInput;
                }

            } else if (command.toLowerCase().equals("b")) {
                // Word guess attempt of user
                System.out.println("\r\nEnter your word: ");
                String inputWord = this.scanner.nextLine();

                if (!inputWord.isEmpty()) {
                    guessWord = inputWord;
                }
            }

            // Check if attempt is valid
            if (letter != ' ' || !guessWord.isEmpty()) {
                // Send attempt to server
                out.writeObject(new Attempt(this.username, letter, guessWord));
                out.flush();

                // Receive attempt info of the server
                attemptData = (Attempt) in.readObject();

                // Print congrats text if word has been guessed by the user
                if (attemptData.hasGuessedTheCorrectWord()) {
                    System.out.printf("\r\n\tCongratulations %s, you have guessed the correct word!",
                            this.username);
                }

                System.out.println("\r\n\tCurrent guessing word state: " + attemptData.getMysteryWord());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Client client = new Client(
                InetAddress.getByName("127.0.0.1"), 8090);

        System.out.println("\r\nConnected to Server: " + client.socket.getInetAddress());
        client.start();
    }
}
