package com.bckho;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GameUtil {
    private File wordsFile;
    private List<String> wordList;
    private Random rand;

    public GameUtil() throws IOException{
        this.wordsFile = new File("words.txt");
        this.wordList = new ArrayList<>();
        this.rand = new Random();
        moveWordsFromFileToWordsList();
    }

    public void moveWordsFromFileToWordsList() throws IOException {
        Scanner scanner = new Scanner(new File("src\\com\\bckho\\words.txt"));
        while (scanner.hasNext()) {
            if (!this.wordList.contains(scanner.nextLine())) {
                this.wordList.add(scanner.nextLine());
            }
        }
    }

    public String getRandomWordFromWordList() {
        int listSize = this.wordList.size();
        int randomNumber = rand.nextInt(listSize);

        return this.wordList.get(randomNumber);
    }
}
