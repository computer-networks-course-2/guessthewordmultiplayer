package com.bckho;

import java.io.Serializable;

public class Attempt implements Serializable {
    String clientUsername;
    char letterGuessed;

    private String wordGuessed;
    private boolean hasGuessedTheCorrectLetter;
    private boolean hasGuessedTheCorrectWord;
    private String mysteryWord;

    public Attempt(
            String clientUsername,
            char letterGuessed,
            String wordGuessed
    ) {
        this.clientUsername = clientUsername;
        this.letterGuessed = letterGuessed;
        this.wordGuessed = wordGuessed;
        this.hasGuessedTheCorrectLetter = false;
        this.hasGuessedTheCorrectWord = false;
        this.mysteryWord = null;
    }

    public String getClientUsername() {
        return clientUsername;
    }

    public char getLetterGuessed() {
        return letterGuessed;
    }

    public String getWordGuessed() {
        return wordGuessed;
    }

    public void setWordGuessed(String wordGuessed) {
        this.wordGuessed = wordGuessed;
    }

    public boolean hasGuessedTheCorrectLetter() {
        return hasGuessedTheCorrectLetter;
    }

    public void setHasGuessedTheCorrectLetter(boolean hasGuessedTheCorrectLetter) {
        this.hasGuessedTheCorrectLetter = hasGuessedTheCorrectLetter;
    }

    public boolean hasGuessedTheCorrectWord() {
        return hasGuessedTheCorrectWord;
    }

    public void setHasGuessedTheCorrectWord(boolean hasGuessedTheCorrectWord) {
        this.hasGuessedTheCorrectWord = hasGuessedTheCorrectWord;
    }

    public String getMysteryWord() {
        return mysteryWord;
    }

    public void setMysteryWord(String mysteryWord) {
        this.mysteryWord = mysteryWord;
    }
}
