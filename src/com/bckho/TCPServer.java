package com.bckho;

import java.io.*;
import java.net.*;

public class TCPServer {

    ServerSocket server;
    GameUtil gameUtil;
    ObjectInputStream inputStream;
    ObjectOutputStream outputStream;

    private Game currentGame;

    public TCPServer() throws Exception {
        this.server = new ServerSocket(8090, 1, InetAddress.getByName("127.0.0.1"));
        this.gameUtil = new GameUtil();
        this.currentGame = returnNewGame();
    }

    private void listen() throws Exception {
        Attempt data;
        Socket client = this.server.accept();
        String clientAddress = client.getInetAddress().getHostAddress();
        System.out.println("\r\nNew connection from " + clientAddress);

        inputStream = new ObjectInputStream(client.getInputStream());
        outputStream = new ObjectOutputStream(client.getOutputStream());

        while (true) {
            // Read Attempt object that has been send from the client
            data = (Attempt) inputStream.readObject();

            // Check if the attempt is valid
            if (!data.getWordGuessed().isEmpty() || data.getLetterGuessed() != ' ') {
                System.out.printf("\r\nAttempt from %s >>> username: %s | letter: %c | word: %s | ",
                        clientAddress,
                        data.getClientUsername(),
                        data.getLetterGuessed(),
                        data.getWordGuessed());

                // Attempt data modified in order to send current state of game
                Attempt cbAttempt = this.currentGame.checkAttemptAndReturn(data);

                // If word has been guessed, then start a new Game
                if (this.currentGame.wordHasBeenGuessed()) {
                    this.currentGame = returnNewGame();
                }

                // Send the modified attempt to client
                outputStream.writeObject(cbAttempt);
                outputStream.flush();
            }
        }
    }

    public Game returnNewGame() {
        return new Game(this.gameUtil.getRandomWordFromWordList());
    }

    public InetAddress getSocketAddress() {
        return this.server.getInetAddress();
    }

    public int getPort() {
        return this.server.getLocalPort();
    }

    public static void main(String[] args) throws Exception {
        TCPServer app = new TCPServer();
        System.out.println("\r\nRunning Server: " +
                "Host=" + app.getSocketAddress().getHostAddress() +
                " Port=" + app.getPort());

        app.listen();
    }
}
